# Flip sign of value v in signed permutation p with unsigned inverse q
def flip(p, q, v):  # with 1-based indexing, ie p[0] and q[0] are ignored.
    p[q[v]] = -p[q[v]]
    return p, q

# 2-twists value v to the left / right using delta = -1 / delta = 1
def twist(p, q, v, delta):  # with 1-based indexing into both p and q.
    pos = q[v]           # Use inverse to get the position of value v.
    u = abs(p[pos+delta]) # Get value to the left or right of value v.
    p[pos], p[pos+delta] = -p[pos+delta], -p[pos]     # Twist u and v.
    q[v], q[u] = pos+delta, pos             # Update unsigned inverse.
    return p, q  # Return signed permutation and its unsigned inverse.

# Generate each signed permutation in worst-case O(1)-time.
def twisted(n):
  m = 2*n-1  # The mixed-radix bases are n, n-1, ..., 2, 1, 2,..., 2.
  bases = tuple(range(n,1,-1)) + (2,) * n     # but the 1 is omitted.
  word = [0] * m            # The mixed-radix word is initially 0^m.
  dirs = [1] * m            # Direction of change for digits in word.
  focus = list(range(m+1))  # Focus pointers select digits to change.
  flips =   [lambda p,q,v=v: flip(p,q,v)     for v in range(n,0,-1)]
  twistsL = [lambda p,q,v=v: twist(p,q,v,-1) for v in range(n,1,-1)]
  twistsR = [lambda p,q,v=v: twist(p,q,v, 1) for v in range(n,1,-1)]
  fns = [None] + twistsL + flips + flips[-1::-1] + twistsR[-1::-1]
  p = [None] + list(range(1,n+1))  # To use 1-based indexing we set
  q = [None] + list(range(1,n+1))  # and ignore p[0] = q[0] = None.
  yield p[1:]  # Pause the function and return signed permutation p.
  while focus[0] < m: # Continue if the digit to change is in word.
    index = focus[0]  # The index of the digit to change in word.
    focus[0] = 0      # Reset the first focus pointer.
    word[index] += dirs[index]  # Adjust the digit using its direction.
    change = dirs[index] * (index+1)  # Note: change can be negative.
    if word[index] == 0 or word[index] == bases[index]-1:  # If the
      focus[index] = focus[index+1] # mixed-radix word's digit is at
      focus[index+1] = index+1      # its min or max value, then update
      dirs[index] = -dirs[index]    # focus pointers, change direction.
    p, q = fns[change](p, q)  # Apply twist or flip encoded by change.
    yield p[1:]

# Demonstrating the use of our twisted function for n = 4.
for p in twisted(4): print(p)  # Print all 2^n n! signed permutations.
