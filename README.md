# signed-plain-changes

These Python files can be used to generate all $2^n n!$ signed permutations using signed versions of plain changes.
In particular, ```twisted.py``` generates $\mathsf{twisted}(n)$ from the paper *Generating Signed Permutations by Twisting Two-Sided Ribbons* by Qiu and Williams from the LATIN 2024 conference.

